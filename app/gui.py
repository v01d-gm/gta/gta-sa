import typing
import tkinter.ttk
import tkinter.messagebox

import trainerbase.gameobject
import trainerbase.scriptengine
import trainerbase.codeinjection
import objects
import teleport
import scripts


def set_entry_text(entry: tkinter.ttk.Entry, text: str):
    entry.delete(0, tkinter.END)
    entry.insert(0, text)


def add_gameobject_to_gui(
    parent_frame: tkinter.ttk.LabelFrame,
    readable_name: str,
    row: int,
    gameobject: trainerbase.gameobject.GameObject,
    before_set: typing.Callable = int,
):
    def on_frozen_state_change():
        gameobject.frozen = gameobject.value if frozen_state.get() else None

    def on_value_get():
        set_entry_text(text, gameobject.value)

    def on_value_set():
        new_value = before_set(text.get())
        if gameobject.frozen is None:
            gameobject.value = new_value
        else:
            gameobject.frozen = new_value

    column = 0

    label = tkinter.ttk.Label(parent_frame, text=readable_name)
    label.grid(row=row, column=column)
    column += 1

    frozen_state = tkinter.BooleanVar(value=gameobject.frozen is not None)
    frozen_checkbutton = tkinter.ttk.Checkbutton(
        parent_frame,
        command=on_frozen_state_change,
        var=frozen_state,
    )
    frozen_checkbutton.grid(row=row, column=column)
    column += 1

    text = tkinter.ttk.Entry(parent_frame, width=20)
    text.grid(row=row, column=column)
    column += 1

    button_get = tkinter.ttk.Button(parent_frame, text="get", command=on_value_get)
    button_get.grid(row=row, column=column)
    column += 1

    button_set = tkinter.ttk.Button(parent_frame, text="set", command=on_value_set)
    button_set.grid(row=row, column=column)


def add_script_to_gui(
    parent_frame: tkinter.ttk.LabelFrame,
    row: int,
    script: trainerbase.scriptengine.Script,
):
    def on_script_state_change():
        script.enabled = script_state.get()

    label = tkinter.ttk.Label(parent_frame, text=script.callback.__name__)
    label.grid(row=row, column=0)

    script_state = tkinter.BooleanVar(value=script.enabled)
    script_checkbutton = tkinter.ttk.Checkbutton(
        parent_frame,
        command=on_script_state_change,
        var=script_state,
    )
    script_checkbutton.grid(row=row, column=1)


def add_codeinjection_to_gui(
    parent_frame: tkinter.ttk.LabelFrame,
    readable_name: str,
    row: int,
    injection: trainerbase.codeinjection.CodeInjection,
):
    def on_codeinjection_state_change():
        if injection_state.get():
            injection.inject()
        else:
            injection.eject()

    label = tkinter.ttk.Label(parent_frame, text=readable_name)
    label.grid(row=row, column=0)

    injection_state = tkinter.BooleanVar(value=False)
    injection_checkbutton = tkinter.ttk.Checkbutton(
        parent_frame,
        command=on_codeinjection_state_change,
        var=injection_state,
    )
    injection_checkbutton.grid(row=row, column=1)


def init_gui() -> tkinter.Tk:
    window = tkinter.Tk()
    window.title("GTA SA Trainer by v01d")
    window.resizable(False, False)

    frame_player = tkinter.ttk.LabelFrame(window, text="Player")
    add_gameobject_to_gui(frame_player, "HP", 0, objects.hp)
    add_gameobject_to_gui(frame_player, "Money", 1, objects.money)
    add_gameobject_to_gui(
        frame_player, "Infinite Stamina (0 or 1)", 2, objects.inf_stamina_flag
    )
    add_gameobject_to_gui(frame_player, "Friend's HP", 3, objects.friend_hp)
    frame_player.grid(row=0, column=0)

    frame_weapons = tkinter.ttk.LabelFrame(window, text="Weapons")
    add_gameobject_to_gui(frame_weapons, "Pistol", 0, objects.pistol)
    add_gameobject_to_gui(frame_weapons, "Shotgun", 1, objects.shotgun)
    add_gameobject_to_gui(frame_weapons, "Submachine", 2, objects.submachine)
    add_gameobject_to_gui(frame_weapons, "Assault", 3, objects.assault)
    add_gameobject_to_gui(frame_weapons, "Sniper", 4, objects.sniper)
    add_gameobject_to_gui(frame_weapons, "Heavy", 5, objects.heavy)
    add_gameobject_to_gui(frame_weapons, "Explosive", 6, objects.explosive)
    add_gameobject_to_gui(frame_weapons, "Tool", 7, objects.tool)
    frame_weapons.grid(row=1, column=0)

    frame_score_helper = tkinter.ttk.LabelFrame(window, text="Score Helper")
    add_gameobject_to_gui(
        frame_score_helper, "Lowrider Mission", 0, objects.lowrider_mission_score
    )
    add_gameobject_to_gui(
        frame_score_helper, "Dancing Mission", 1, objects.dancing_mission_score
    )
    frame_score_helper.grid(row=2, column=0)

    frame_collectables = tkinter.ttk.LabelFrame(window, text="Collectables")
    add_gameobject_to_gui(frame_collectables, "Graffiti", 0, objects.graffiti)
    add_gameobject_to_gui(frame_collectables, "Horseshoes", 1, objects.horseshoes)
    add_gameobject_to_gui(frame_collectables, "Oysters", 2, objects.oysters)
    frame_collectables.grid(row=0, column=1)

    frame_girlfriend_progress = tkinter.ttk.LabelFrame(
        window, text="Girlfriend Progress"
    )
    add_gameobject_to_gui(frame_girlfriend_progress, "Denise", 0, objects.denise)
    add_gameobject_to_gui(frame_girlfriend_progress, "Michelle", 1, objects.michelle)
    add_gameobject_to_gui(frame_girlfriend_progress, "Helen", 2, objects.helen)
    add_gameobject_to_gui(frame_girlfriend_progress, "Barbara", 3, objects.barbara)
    add_gameobject_to_gui(frame_girlfriend_progress, "Katie", 4, objects.katie)
    add_gameobject_to_gui(frame_girlfriend_progress, "Millie", 5, objects.millie)
    frame_girlfriend_progress.grid(row=1, column=1)

    frame_other = tkinter.ttk.LabelFrame(window, text="Other")
    add_gameobject_to_gui(frame_other, "Gravity", 0, objects.gravity, float)
    frame_other.grid(row=2, column=1)

    def on_get_coords():
        x, y, z = teleport.tp.get_coords()
        set_entry_text(text_player_x, x)
        set_entry_text(text_player_y, y)
        set_entry_text(text_player_z, z)

    def on_set_coords():
        x = float(text_player_x.get())
        y = float(text_player_y.get())
        z = float(text_player_z.get())
        teleport.tp.set_coords(x, y, z)

    def on_goto_label():
        teleport.tp.goto(combobox_tp_label.get())

    def on_clip_coords():
        window.clipboard_clear()
        window.clipboard_append(repr(teleport.tp.get_coords()))

    frame_teleport = tkinter.ttk.LabelFrame(window, text="Teleport")
    label_player_x = tkinter.ttk.Label(frame_teleport, text="X")
    label_player_y = tkinter.ttk.Label(frame_teleport, text="Y")
    label_player_z = tkinter.ttk.Label(frame_teleport, text="Z")
    label_player_x.grid(row=0, column=0)
    label_player_y.grid(row=1, column=0)
    label_player_z.grid(row=2, column=0)
    text_player_x = tkinter.ttk.Entry(frame_teleport, width=20)
    text_player_y = tkinter.ttk.Entry(frame_teleport, width=20)
    text_player_z = tkinter.ttk.Entry(frame_teleport, width=20)
    text_player_x.grid(row=0, column=1)
    text_player_y.grid(row=1, column=1)
    text_player_z.grid(row=2, column=1)
    button_get_coords = tkinter.ttk.Button(
        frame_teleport, text="get", command=on_get_coords
    )
    button_set_coords = tkinter.ttk.Button(
        frame_teleport, text="set", command=on_set_coords
    )
    button_get_coords.grid(row=3, column=0)
    button_set_coords.grid(row=3, column=1)
    combobox_tp_label = tkinter.ttk.Combobox(
        frame_teleport, values=list(teleport.tp.labels.keys()), state="readonly"
    )
    combobox_tp_label.current(0)
    combobox_tp_label.grid(row=4, column=1)
    button_goto = tkinter.ttk.Button(frame_teleport, text="Goto", command=on_goto_label)
    button_goto.grid(row=4, column=0)
    button_clip_coords = tkinter.ttk.Button(
        frame_teleport, text="Clip Coords", command=on_clip_coords
    )
    button_clip_coords.grid(row=5, column=0)
    frame_teleport.grid(row=0, column=2)

    def on_scriptengine_check():
        if scripts.script_engine.should_run:
            tkinter.messagebox.showinfo("ScriptEngine", "It's OK!")
        else:
            tkinter.messagebox.showerror("ScriptEngine", "Stopped.")

    frame_scripts = tkinter.ttk.LabelFrame(window, text="Scripts")
    for row, script in enumerate(scripts.script_engine.scripts):
        add_script_to_gui(frame_scripts, row, script)
    check_scriptengine_status = tkinter.ttk.Button(
        frame_scripts, text="Check ScriptEngine", command=on_scriptengine_check
    )
    check_scriptengine_status.grid(row=row + 1, column=0)
    frame_scripts.grid(row=1, column=2)

    # CodeInjection
    frame_codeinjection = tkinter.ttk.LabelFrame(window, text="Code Injection")
    # add_codeinjection_to_gui(...)
    frame_codeinjection.grid(row=1, column=0)

    return window
