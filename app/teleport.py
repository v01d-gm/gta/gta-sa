from trainerbase.gameobject import GameFloat

import objects


class Teleport:
    def __init__(
        self,
        player_x: GameFloat,
        player_y: GameFloat,
        player_z: GameFloat,
        labels: dict[str, tuple[int]] = None,
    ):
        self.player_x = player_x
        self.player_y = player_y
        self.player_z = player_z
        self.labels = {} if labels is None else labels

    def set_coords(self, x: float, y: float, z: float = 100):
        self.player_x.value = x
        self.player_y.value = y
        self.player_z.value = z

    def get_coords(self):
        return self.player_x.value, self.player_y.value, self.player_z.value

    def goto(self, label: str):
        self.set_coords(*self.labels[label])


tp = Teleport(
    objects.player_x,
    objects.player_y,
    objects.player_z,
    {
        "ls_cj_home": (2497.61962890625, -1680.79443359375, 13.35544490814209),
        "ls_bigsmoke_home": (2074.367919921875, -1705.9500732421875, 13.546875),
        "ls_gym": (2223.778564453125, -1723.2047119140625, 13.5625),
        "countryside_tt": (-2197.495849609375, -2270.173828125),
        "angel_pine": (-2040.8585205078125, -2534.267578125),
        "catalina_home": (873.14404296875, -35.56463623046875, 63.195316314697266),
        "blueberry": (300.10345458984375, -147.6251983642578, 1.578125),
        "whetstone": (-1442.2012939453125, -1540.303955078125, 101.7578125),
        "doherty": (-2025.184326171875, 142.05274963378906, 28.8359375),
        "sf_zero": (-2244.1259765625, 137.9945831298828, 35.3203125),
        "sf_jizzy": (-2615.367919921875, 1400.979736328125, 7.09375),
        "ls_vinewood_sub": (788.3727416992188, -1326.5211181640625, -0.507807731628418),
        "sf_minigun": (-1498.5198974609375, 595.135986328125, 42.2578125),
        "sf_woozy": (-2158.75146484375, 654.3323974609375, 52.3671875),
        "toreno": (-688.6341552734375, 952.3500366210938, 12.160249710083008),
        "eldiablo": (-429.86279296875, 2214.205322265625, 42.4296875),
        "aerodrome": (422.3405456542969, 2547.239990234375, 16.294376373291016),
        "lv_caligula": (2193.43408203125, 1673.0751953125, 12.3671875),
        "lv_4dragons": (2030.1787109375, 996.2227172851562, 10.813100814819336),
        "lv_millie": (2043.4158935546875, 2734.246826171875, 10.8203125),
        "lv_hospital": (1610.59033203125, 1846.537109375, 10.82027530670166),
        "lv_abattoir": (989.524169921875, 2149.445068359375, 10.8203125),
        "zone69": (214.56887817382812, 1906.5419921875, 17.640625),
        "lv_gym": (1953.16796875, 2292.08154296875, 10.8203125),
        "lc_step1": (1415.31298828125, 2.30586886405945, 1000.92565917969),
        "lc_step2": (-736.078857421875, 496.183410644531, 1371.9765625),
        "lc_step3": (-785.00, 505.52, 1371.74),
        "ls_maddog": (1261.640625, -790.5574951171875, 92.03125),
        "lastmission_floor0": (2538.899169921875, -1289.27099609375, 34.9453125),
        "lastmission_floor1": (2550.86865234375, -1292.4278564453125, 1031.421875),
        "lastmission_bigsmoke": (2567.918701171875, -1285.4766845703125, 1060.984375),
    },
)
